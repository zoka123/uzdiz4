/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import zantolov_zadaca_4.Car;
import zantolov_zadaca_4.parking.ParkingLot;
import zantolov_zadaca_4.parking.ParkingSpace;
import zantolov_zadaca_4.parking.ParkingZone;

/**
 *
 * @author zoka123
 */
public class CarsController extends Controller {

    public ParkingLot lot;

    @Override
    public void run() {

        Car currentCar = null;

        while (true) {
            String content = "";
            currentCar = lot.getNextUnparkedCar();
            if (currentCar != null) {
                content += "Automobil: " + currentCar.index;

                ParkingZone zone = lot.getRandomZone();
                ParkingSpace space = zone.getFreeParkingSpace();

                if (space != null) {
                    space.setParkedCar(currentCar);
                    currentCar.space = space;

                    content += " se parkira na " + space.index
                            + " u zoni " + zone.index
                            + " cijena: " + space.getPrice(zone.index, lot.cijenaJedinice, lot.zones.size()) + "kn"
                            + " u " + space.start;


                } else {
                    content += " nije pronašao mjesto";
                }

                this.getModel().setContent(content);
            } else {
                content += " currentCar = null";
            }

            try {
                Thread.sleep(lot.getCarParkingEnterTimeout() * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CarsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
