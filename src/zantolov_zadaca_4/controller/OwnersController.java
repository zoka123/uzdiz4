/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import zantolov_zadaca_4.Car;
import zantolov_zadaca_4.parking.ParkingLot;

/**
 *
 * @author zoka123
 */
public class OwnersController extends Controller {

    public ParkingLot lot;

    @Override
    public void run() {
        Car currentCar = null;

        while (true) {
            String content = "";
            currentCar = lot.getNextParkedCar();
            if (currentCar != null) {
                content += "Vlasnik automobila: " + currentCar.index;

                /**
                 * 0 - ništa, 1 - izaći, 2 - produljiti parkiranje,
                 *
                 * generiranaVrijednost4 tako da vrijednost 0 ima 25%, 1 ima 50%
                 * i 2 ima 25%
                 */
                
                int choice = (int) (Math.random() * 100);
                if (choice <= 25) {
                    // 0 - ništa
                    content += " ne radi ništa";
                } else if (choice > 25 && choice <= 75) {
                    // 1 - izaći
                    content += " izlazi";
                    
                    
                    
                } else if (choice > 75) {
                    // 2 - produljiti
                    content += " produljuje parking";
                }
            } else {
                
            }

            this.getModel().setContent(content);

            try {
                Thread.sleep(lot.getCarParkingExitTimeout() * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CarsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
