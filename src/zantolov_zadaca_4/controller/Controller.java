/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.controller;

import zantolov_zadaca_4.model.Model;
import zantolov_zadaca_4.view.View;

/**
 *
 * @author zoka123
 */
public class Controller implements Runnable {
    
    private View view;
    
    private Model model;
    
    public View getView() {
        return view;
    }
    
    public void setView(View view) {
        this.view = view;
    }
    
    public Model getModel() {
        return model;
    }
    
    public void setModel(Model model) {
        this.model = model;
    }
    
    @Override
    public void run() {
        model.addObserver(view);
    }
    
}
