/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.parking;

import java.util.Date;
import zantolov_zadaca_4.Car;

/**
 *
 * @author zoka123
 */
public class ParkingSpace {

    public int index;

    public Car parkedCar = null;

    public Date start;

    public boolean isTaken() {
        return this.parkedCar != null;
    }

    public void setParkedCar(Car car) {
        this.parkedCar = car;
        this.start = new Date();
    }

    public long getPrice(int zoneIndex, int cijenaJedinice, int brojZona) {
        return ((brojZona + 1 - zoneIndex) * cijenaJedinice);
    }

}
