/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.parking;

import java.util.ArrayList;

/**
 *
 * @author zoka123
 */
public class ParkingZone {

    public int index;

    public int kapacitet;

    public int vrijemeParkiranja;

    public volatile ArrayList<ParkingSpace> spaces = new ArrayList<>();

    public ParkingSpace getFreeParkingSpace() {
        for (ParkingSpace ps : spaces) {
            if (ps.isTaken() == false) {
                return ps;
            }
        }
        return null;
    }
}
