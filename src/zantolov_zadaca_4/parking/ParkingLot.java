/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.parking;

import java.util.ArrayList;
import zantolov_zadaca_4.Car;

/**
 *
 * @author zoka123
 */
public class ParkingLot {

    public ArrayList<Car> cars = new ArrayList<>();

    public ArrayList<ParkingZone> zones = new ArrayList<>();

    public Integer vremenskaJedinica;
    public Integer intervalDolaska;
    public Integer intervalOdlaska;
    public Integer cijenaJedinice;

    private int carSeekIndexStart = 0;
    private int parkedCarSeekIndexStart = 0;

    public int getCarParkingEnterTimeout() {
        int sleepTime = (int) Math.round((vremenskaJedinica / intervalDolaska) * Math.random());
        return sleepTime;
    }

    public int getCarParkingExitTimeout() {
        int sleepTime = (int) Math.round((vremenskaJedinica / intervalOdlaska) * Math.random());
        return sleepTime;
    }

    public ParkingZone getRandomZone() {
        int brojZona = zones.size();
        int temp = (int) Math.round(Math.random() * (brojZona - 1));
        return this.zones.get(temp);
    }

    public Car getNextUnparkedCar() {
        int bottom = carSeekIndexStart;
        int up = carSeekIndexStart + cars.size() - 1;
        for (int i = bottom; i < up; i++) {
            Car c = cars.get(i % cars.size());
            if (c.isParked() == false) {
                this.carSeekIndexStart = (i + 1) % cars.size();
                return c;
            }
        }

        return null;
    }

    public Car getNextParkedCar() {
        int bottom = parkedCarSeekIndexStart;
        int up = parkedCarSeekIndexStart + cars.size() - 1;
        for (int i = bottom; i < up; i++) {
            Car c = cars.get(i % cars.size());
            if (c.isParked() == true) {
                parkedCarSeekIndexStart = (i + 1) % cars.size();
                return c;
            }
        }

        return null;
    }

    public int getParkingPrice(int i) {
        return ((zones.size() + 1 - i) * cijenaJedinice);
    }

}
