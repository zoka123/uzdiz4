/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.model;

import java.util.Observable;

/**
 *
 * @author zoka123
 */
public class Model extends Observable {

    private String content;

    public String getContent() {
        return this.content;
    }

    public synchronized void setContent(String content) {
        this.content = content;

        if (content != null) {
            this.setChanged();
            this.notifyObservers();
        }
    }
}
