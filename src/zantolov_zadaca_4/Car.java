/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4;

import zantolov_zadaca_4.parking.ParkingSpace;

/**
 *
 * @author zoka123
 */
public class Car {

    public int index;

    public ParkingSpace space = null;

    public Car(int index) {
        this.index = index;
    }

    public boolean isParked() {
        return this.space != null;
    }

}
