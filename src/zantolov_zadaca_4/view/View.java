/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4.view;

import java.util.Observable;
import java.util.Observer;
import zantolov_zadaca_4.model.Model;

/**
 *
 * @author zoka123
 */
public class View implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Model) {
            System.out.println(((Model) o).getContent());
            ((Model) o).setContent(null);
        }

    }

}
