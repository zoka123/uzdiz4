package zantolov_zadaca_4;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author zoka123
 */
public class ArgumentValidator {

    public Integer brojAutomobila;
    public Integer brojZona;
    public Integer kapacitetZone;
    public Integer maksParkiranje;
    public Integer vremenskaJedinica;
    public Integer intervalDolaska;
    public Integer intervalOdlaska;
    public Integer cijenaJedinice;
    public Integer intervalKontrole;
    public Integer kaznaParkiranja;

    public boolean validate(String[] args) throws Exception {

        // count
        if (args.length != 10) {
            throw new Exception("Nedovoljan broj argumenata");
        }

        brojAutomobila = Integer.parseInt(args[0]);
        brojZona = Integer.parseInt(args[1]);
        kapacitetZone = Integer.parseInt(args[2]);
        maksParkiranje = Integer.parseInt(args[3]);
        vremenskaJedinica = Integer.parseInt(args[4]);
        intervalDolaska = Integer.parseInt(args[5]);
        intervalOdlaska = Integer.parseInt(args[6]);
        cijenaJedinice = Integer.parseInt(args[7]);
        intervalKontrole = Integer.parseInt(args[8]);
        kaznaParkiranja = Integer.parseInt(args[9]);

        /*
         brojAutomobila = 10 - 100
         brojZona = 1 - 4
         kapacitetZone = 1 - 100
         maksParkiranje = 1 - 10
         vremenskaJedinica = 1 - 10 sek
         intervalDolaska = 1 - 10
         intervalOdlaska = 1 - 10
         cijenaJedinice = 1 - 10 kn
         intervalKontrole = 1 - 10
         kaznaParkiranja = 10 - 100
         */
        // brojAutomobila = 10 - 100
        if (brojAutomobila < 10 || brojAutomobila > 100) {
            throw new Exception("Broj automobila mora biti u intervalu 10 - 100");
        }

        // brojZona = 1 - 4
        if (brojZona < 1 || brojZona > 4) {
            throw new Exception("Broj zona mora biti u intervalu 1 - 4");
        }

        // kapacitetZone = 10 - 100
        if (kapacitetZone < 10 || kapacitetZone > 100) {
            throw new Exception("Kapacitet zone mora biti u intervalu 10 - 100");
        }

        // maksParkiranje = 1 - 10
        if (maksParkiranje < 1 || maksParkiranje > 10) {
            throw new Exception("Maks parkiranje mora biti u intervalu 1 - 10");
        }

        // vremenskaJedinica = 1 - 10
        if (vremenskaJedinica < 1 || vremenskaJedinica > 10) {
            throw new Exception("vremenskaJedinica mora biti u intervalu 1 - 10");
        }

        // intervalDolaska = 1 - 10
        if (intervalDolaska < 1 || intervalDolaska > 10) {
            throw new Exception("intervalDolaska mora biti u intervalu 1 - 10");
        }

        // intervalOdlaska = 1 - 10
        if (intervalOdlaska < 1 || intervalOdlaska > 10) {
            throw new Exception("intervalOdlaska mora biti u intervalu 1 - 10");
        }

        // cijenaJedinice = 1 - 10
        if (cijenaJedinice < 1 || cijenaJedinice > 10) {
            throw new Exception("cijenaJedinice mora biti u intervalu 1 - 10");
        }

        // intervalKontrole
        if (intervalKontrole < 1 || intervalKontrole > 10) {
            throw new Exception("intervalKontrole mora biti u intervalu 1 - 10");
        }

        // kaznaParkiranja
        if (kaznaParkiranja < 10 || kaznaParkiranja > 100) {
            throw new Exception("kaznaParkiranja mora biti u intervalu 10 - 100");
        }

        return false;
    }

}
