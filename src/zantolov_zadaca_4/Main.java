/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zantolov_zadaca_4;

import zantolov_zadaca_4.controller.CarsController;
import zantolov_zadaca_4.controller.Controller;
import zantolov_zadaca_4.controller.OwnersController;
import zantolov_zadaca_4.model.Model;
import zantolov_zadaca_4.parking.ParkingLot;
import zantolov_zadaca_4.parking.ParkingSpace;
import zantolov_zadaca_4.parking.ParkingZone;
import zantolov_zadaca_4.view.View;

/**
 *
 * @author zoka123
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ParkingLot parking = new ParkingLot();
        Model viewModel = new Model();
        View uiView = new View();

        Controller viewController = new Controller();
        viewController.setModel(viewModel);
        viewController.setView(uiView);

        Thread uiThred = new Thread(viewController, "thread.ui");
        uiThred.start();

        ArgumentValidator validator = new ArgumentValidator();

        try {
            validator.validate(args);
        } catch (Exception e) {
            viewModel.setContent(e.getMessage());
            System.exit(1);
        }

        parking.vremenskaJedinica = validator.vremenskaJedinica;
        parking.cijenaJedinice = validator.cijenaJedinice;
        parking.intervalDolaska = validator.intervalDolaska;
        parking.intervalOdlaska = validator.intervalOdlaska;

        for (int i = 1; i <= validator.brojAutomobila; i++) {
            parking.cars.add(new Car(i));
        }

        for (int i = 1; i <= validator.brojZona; i++) {
            ParkingZone zone = new ParkingZone();
            zone.kapacitet = i * validator.kapacitetZone;
            zone.vrijemeParkiranja = i * validator.maksParkiranje * validator.vremenskaJedinica;
            zone.index = i;

            for (int j = 1; j <= validator.kapacitetZone; j++) {
                ParkingSpace space = new ParkingSpace();
                space.index = i;
                zone.spaces.add(space);
            }

            parking.zones.add(zone);
        }

        CarsController carsCtl = new CarsController();
        carsCtl.setView(uiView);
        carsCtl.setModel(viewModel);
        carsCtl.lot = parking;

        OwnersController ownersCtl = new OwnersController();
        ownersCtl.setView(uiView);
        ownersCtl.setModel(viewModel);
        ownersCtl.lot = parking;

        Thread carsThread = new Thread(carsCtl, "thread.cars");
        carsThread.start();

        Thread owThread = new Thread(ownersCtl, "thread.owners");
        owThread.start();

    }

}
